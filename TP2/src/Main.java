import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        String input = null;
        String [] parts;
        BTree binaryTree = new BTree();
        Node aux;

        while(!(input = sc.nextLine()).equals("")) {
            parts = input.split(" ");
            if(parts[0].compareTo("STATUS") == 0) {
                if((aux = binaryTree.findNode(parts[1], binaryTree.root)) != null) {
                    System.out.println(aux.getMatricula() + " " + aux.getPass() + " " + aux.getStatus());
                } else {
                    System.out.println(parts[1] + " NO RECORD");
                }
            } else if(parts[0].compareTo("PASS") == 0) {
                if((aux = binaryTree.findNode(parts[1], binaryTree.root)) == null) {
                    binaryTree.root = binaryTree.addNode(parts[1], parts[2], binaryTree.root);
                } else {
                    aux.passedAgain();
                    aux.setStatus(parts[2]);
                }
            } else if(parts[0].compareTo("UNFLAG") == 0) {
                if ((aux = binaryTree.findNode(parts[1], binaryTree.root)) != null) {
                    aux.setStatus("R");
                }
            }
        }
    }
}

class BTree {
    public Node root;

    public BTree() {
        this.root = null;
    }

    public Node addNode(String matricula, String status, Node rewt) {
        if(rewt == null) {
            rewt = new Node(matricula, status);
        }

        else if(matricula.compareTo(rewt.matricula) < 0) {
            rewt.leftChild = addNode(matricula, status, rewt.leftChild);
        } else if(matricula.compareTo(rewt.matricula) > 0) {
            rewt.rightChild = addNode(matricula, status, rewt.rightChild);
        }
        return rewt;
    }

    public Node findNode(String matricula, Node rewt) {
        while(rewt != null) {
            if(matricula.compareTo(rewt.matricula) < 0) {
                rewt =  rewt.leftChild;
            } else if(matricula.compareTo(rewt.matricula) > 0){
                rewt = rewt.rightChild;
            }
            else {
                return rewt;
            }
        }
        return null;
    }
}


class Node {
    public int pass;
    public String matricula;
    public String status;
    public Node leftChild;
    public Node rightChild;

    public Node(String matricula, String status) {
        this.pass = 1;
        this.matricula = matricula;
        this.status = status;
    }

    public void passedAgain() {
        pass++;
    }

    public int getPass() {
        return this.pass;
    }

    public String getMatricula() {
        return this.matricula;
    }

    public String getStatus(){
        return this.status;
    }

    public void setStatus(String status) {
       this.status = status;
    }
}
