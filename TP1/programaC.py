import time

def calculate_min_i(array, inpt, i, j, size):
    aux=i+1
    aux1=i+2
    aux2=j-1
    aux3=j-2
    if(aux > size):
        aux=0
    if(aux1 > size):
        aux1=0
    if(aux2 < 0):
        aux2=0
    if(aux3 < 0):
        aux3=0
    vi_a1=array[aux1][j]
    vi_a2=array[aux][aux2]

    if(vi_a1 > vi_a2):
        resulti = vi_a2
    else:
        resulti = vi_a1
    resulti+=inpt[i]

    return resulti


def calculate_min_j(array, inpt, i, j, size):
    aux=i+1
    aux1=i+2
    aux2=j-1
    aux3=j-2

    if(aux > size):
        aux=0
    if(aux1 > size):
        aux1=0
    if(aux2 < 0):
        aux2=0
    if(aux3 < 0):
        aux3=0
    vj_a1=array[aux][aux2]
    vj_a2=array[i][aux3]

    if(vj_a1 > vj_a2):
        resultj = vj_a2
    else:
        resultj = vj_a1
    resultj+=inpt[j]

    return resultj

def array_result(array, inpt, tamanho):
    aux=1
    for j in range(aux, tamanho):
        for i in range(0,tamanho-aux):
            if(j > i):
                result1=calculate_min_i(array, inpt, i, j, tamanho-1)
                result2=calculate_min_j(array, inpt, i, j, tamanho-1)
                if(result1 > result2):
                    array[i][j] = result1
                else:
                    array[i][j] = result2
            j+=1
        aux+=1
        j=aux

    return array[0][tamanho-1]

if __name__=='__main__':
    lista=[]
    array=[]
    start_time=time.time()
    tamanho=int(raw_input(""))
    for i in range(tamanho):
        lista.append(int(raw_input("")))
    for i in range(tamanho):
        aux=[]
        for j in range(tamanho):
            if(j==i):
                aux.append(lista[i])
            else:
                aux.append(0)
        array.append(aux)
    print(array_result(array, lista, tamanho))
    print('%s seconds' % '%0.15f' % (time.time()-start_time))
