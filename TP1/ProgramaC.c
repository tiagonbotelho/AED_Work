#include <stdio.h>
#include <stdlib.h>

int calculateMini(int array[][], int input[], int i, int j, int tamanho) {
  int resulti;
  int vi_a1;
  int vi_a2;
  int aux=i+1;
  int aux1=i+2;
  int aux2=j-1;
  int aux3=j-2;
  if(aux > tamanho) {
    aux = 0;
  }
  if(aux1 > tamanho) {
    aux1=0;
  }
  if(aux2 < 0) {
    aux2 = 0;
  }
  if(aux3 < 0) {
    aux3 = 0;
  }
  vi_a1 = array[j][aux1];
  vi_a2 = array[aux2][aux1];
  if(vi_a1 > vi_a2) {
    resulti = vi_a2;
  } else {
    resulti =  vi_a1;
  }

  resulti += input[i];
  return resulti;
}

int calculateMinij(int array[][], int input[], int i, int j, int tamanho) {
  int resultj;
  int vj_a1;
  int vj_a2;
  int aux=i+1;
  int aux1=i+2;
  int aux2=j-1;
  int aux3=j-2;

  if(aux > tamanho) {
    aux = 0;
  }
  if(aux1 > tamanho) {
    aux1=0;
  }
  if(aux2 < 0) {
    aux2 = 0;
  }
  if(aux3 < 0) {
    aux3 = 0;
  }

  vj_a1 = array[aux2][aux];
  vj_a2 = array[aux3][i];
  if(vj_a1 > vj_a2) {
    resultj = vj_a2;
  } else {
    resultj = vj_a1;
  }

  resultj+= input[j];
  return resultj;
}

int arrayResult(int array[][], int input[], int tamanho) {
  int i;
  int j;
  int aux=1;

  for(j = 0; j < tamanho; j++) {
    for(i = 0; i < tamanho; i++) {
      if(i > j) {
        array[j][i] = 0;
      } else if(j == i) {
        array[j][i] = input[j];
      }
    }
  }

  for(j = aux; j < tamanho; j++) {
    for(i = 0; i < tamanho-aux; i++) {
      if(j > i){
      if(calculateMini(array, input, i, j, tamanho) < calculateMinij(array, input, i, j, tamanho)) {
          array[j][i] = calculateMinij(array, input, i, j, tamanho);
        } else {
        array[j][i] = calculateMini(array, input, i, j, tamanho);
      }
    }
    j++;
    }
    aux++;
    j=aux;
  }

  return array[tamanho, 0];
}

int main() {
  int tamanho;
  scanf("%d", &tamanho);
  int array[tamanho][tamanho];
  int i = 0;
  int vagoes[tamanho];
  int final_value;
  for(i = 0; i < tamanho; i++) {
    scanf("%d", &vagoes[i]);
  }

  final_value = arrayResult(array, vagoes, tamanho);
  printf("%d\n", final_value);
  return 0;
}
