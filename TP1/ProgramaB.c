#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int maxOf(int value1, int value2) {
  if(value1 > value2) {
    return value1;
  } else {
    return value2;
  }
}

int minOf(int value1, int value2) {
  if(value1 < value2) {
    return value1;
  } else {
    return value2;
  }
}

int bestOption(int array[], int i, int j) {
  if(i == j-1) {
    return maxOf(array[i], array[j]);
  }
  else if(i == j) {
    return array[i];
  }
  else {
    return  maxOf(array[i] + minOf(bestOption(array, i+2, j), bestOption(array, i+1, j-1)), array[j] + minOf(bestOption(array, i+1, j-1), bestOption(array, i, j-2)));
  }
}

int main() {
  int tamanho;
  scanf("%d", &tamanho);
  int i = 0;
  int vagoes[tamanho];
  int final_value;
  clock_t begin = clock();
  clock_t end;
  for(i = 0; i < tamanho; i++) {
    scanf("%d", &vagoes[i]);
  }
  i = 0;
  final_value = bestOption(vagoes, i, tamanho-1);
  printf("%d\n", final_value);
  end = clock();
  double elapsed_secs = (double)(end - begin) / CLOCKS_PER_SEC * 1000;
  printf("%lf\n", elapsed_secs);
  return 0;
}
