import java.util.*;

class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = null;
        String[] parts;
        Node aux;
        Tree rbtree = new Tree();

        while (!(input = sc.nextLine()).equals("")) {
            parts = input.split(" ");
            if (parts[0].compareTo("STATUS") == 0) {
                if ((aux = rbtree.findNode(parts[1], rbtree.root)) != null) {
                    System.out.println(aux.getMatricula() + " " + aux.getPass() + " " + aux.getStatus());
                } else {
                    System.out.println(parts[1] + " NO RECORD");
                }
            } else if (parts[0].compareTo("PASS") == 0) {
                if ((aux = rbtree.findNode(parts[1], rbtree.root)) == null) {
                    rbtree.root = rbtree.insert(parts[1], parts[2], rbtree.root);
                } else {
                    aux.passedAgain();
                    aux.setStatus(parts[2]);
                }
            } else if (parts[0].compareTo("UNFLAG") == 0) {
                if ((aux = rbtree.findNode(parts[1], rbtree.root)) != null) {
                    aux.setStatus("R");
                }
            }
        }
    }
}

class Node {
  public boolean color;
  public int pass;
  public Node left;
  public Node right;
  public String matricula;
  public String status;

  public Node(String matricula, String status, boolean color) {
      this.matricula = matricula;
      this.status = status;
      this.pass = 1;
      this.color = color;
  }

  public String getMatricula() {
    return this.matricula;
  }

  public boolean getColor() {
    return this.color;
  }

  public String getStatus() {
    return this.status;
  }

  public void passedAgain() {
    this.pass++;
  }

  public void setColor(boolean number) {
    this.color = number;
  }

  public void setStatus(String status) {
    this.status = status;
  }
    public int getPass() {
        return this.pass;
    }
}

 class Tree {
  Node root;
  public static final boolean RED = true;
  public static final boolean BLACK = false;

  public Tree() {
    root = null;
  }

  public boolean isRed(Node current) {
    if(current == null) {
      return false;
    }

    return (current.color == RED);
  }

  public void insert(String matricula, String status) {
    root = insert(matricula , status, root);
    root.color = BLACK;
  }

  public Node insert(String matricula, String status, Node root) {
    if(root == null) {
      root = new Node(matricula, status, RED);
    } else if(matricula.compareTo(root.matricula) < 0) {
      root.left = insert(matricula, status, root.left);
    } else if(matricula.compareTo(root.matricula) > 0) {
      root.right = insert(matricula, status, root.right);
    }

    if(isRed(root.right) && !isRed(root.left)) {
     root = rotateLeft(root); 
    }
    
    if(isRed(root.left) && isRed(root.left.left)) {
      root = rotateRight(root);
    }

    if(isRed(root.left) && isRed(root.right)) {
      reColor(root);
    }
    return root;
  }

  public Node rotateLeft(Node root) {
    Node x = root.right;
    root.right = x.left;
    x.left = root;
    x.color = x.left.color;
    x.left.color = RED;
    return x;
  }

  public Node rotateRight(Node root) {
    Node x = root.left;
    root.left = x.right;
    x.right = root;
    x.color = x.right.color;
    x.right.color = RED;
    return x;
  }

  public void reColor(Node root) {
    root.color = !root.color;
    root.left.color = !root.left.color;
    root.right.color = !root.right.color;
  }

  public Node findNode(String matricula, Node root) {
    while(root != null) {
      if(matricula.compareTo(root.matricula) < 0) {
        root = root.left;
      } else if(matricula.compareTo(root.matricula) > 0) {
        root = root.right;
      } else {
        return root;
      }
    }
    return null;
  }
}
