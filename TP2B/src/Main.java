import java.util.*;


public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = null;
        String [] parts;
        Node aux;
        AVLTree avltree = new AVLTree();

        while(!(input = sc.nextLine()).equals("")) {
            parts = input.split(" ");
            if(parts[0].compareTo("STATUS") == 0) {
                if((aux = avltree.findNode(parts[1], avltree.root)) != null) {
                    System.out.println(aux.getMatricula() + " " + aux.getPass() + " " + aux.getStatus());
                } else {
                    System.out.println(parts[1] + " NO RECORD");
                }
            } else if(parts[0].compareTo("PASS") == 0) {
                if((aux = avltree.findNode(parts[1], avltree.root)) == null) {
                    avltree.root = avltree.insert(parts[1], parts[2], avltree.root);
                } else {
                    aux.setPass();
                    aux.setStatus(parts[2]);
                }
            } else if(parts[0].compareTo("UNFLAG") == 0) {
                if((aux = avltree.findNode(parts[1], avltree.root)) != null) {
                    aux.setStatus("R");
                }
            }
        }
    }
}

class Node {

    public Node rightNode;
    public Node leftNode;
    public int height;
    public int pass;
    public String matricula;
    public String status;

    public Node(String matricula, String status) {
        this.pass = 1;
        this.matricula = matricula;
        this.status = status;
        this.height = 0;
    }

    public void setPass() {
        this.pass++;
    }

    public int getPass() {
        return this.pass;
    }

    public String getMatricula() {
        return this.matricula;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}


class AVLTree {

    public Node root;

    public AVLTree() {
        this.root = null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public void insert(String matricula, String status) {
        root = insert(matricula, status, root);
    }

    private int height(Node focus) {

        if(focus == null) {
            return -1;
        } else {
            return focus.height;
        }
    }

    private int max(int leftHeight, int rightHeight) {

        if(leftHeight > rightHeight) {
            return leftHeight;
        } else {
            return rightHeight;
        }
    }

    public Node insert(String matricula, String status, Node root) {
        if (root == null) {
            root = new Node(matricula, status);
        } else if(matricula.compareTo(root.matricula) < 0) {
            root.leftNode = insert(matricula, status, root.leftNode);
            if (height(root.leftNode) - height(root.rightNode) == 2) {
                if (matricula.compareTo(root.leftNode.matricula) < 0) {
                    root = singleLeftChild(root);
                } else {
                    root = doubleLeftChild(root);
                }
            }
        } else if(matricula.compareTo(root.matricula) > 0) {
            root.rightNode = insert(matricula, status, root.rightNode);
            if(height(root.rightNode) - height(root.leftNode) == 2) {
                if(matricula.compareTo(root.rightNode.matricula) > 0) {
                    root = singleRightChild(root);
                } else {
                    root = doubleRightChild(root);
                }
            }
        }
        root.height = max(height(root.leftNode), height(root.rightNode)) + 1;
        return root;
    }

    private Node singleRightChild(Node no) {
        Node no2 = no.rightNode;
        no.rightNode = no2.leftNode;
        no2.leftNode = no;
        no.height = max(height(no.leftNode), height(no.rightNode)) + 1;
        no2.height = max(height(no2.rightNode), no.height) + 1;
        return no2;
    }

    private Node singleLeftChild(Node no) {
        Node no2 = no.leftNode;
        no.leftNode = no2.rightNode;
        no2.rightNode = no;
        no.height = max(height(no.leftNode), height(no.rightNode)) + 1;
        no2.height = max(height(no2.leftNode), no.height) + 1;
        return no2;
    }

    private Node doubleLeftChild(Node no3) {
        no3.leftNode = singleRightChild(no3.leftNode);
        return singleLeftChild(no3);
    }

    private Node doubleRightChild(Node no) {
        no.rightNode = singleLeftChild(no.rightNode);
        return singleRightChild(no);
    }

    public Node findNode(String matricula, Node root) {
        while(root != null) {
            if(matricula.compareTo(root.matricula) < 0) {
                root = root.leftNode;
            } else if(matricula.compareTo(root.matricula) > 0) {
                root = root.rightNode;
            } else {
                return root;
            }
        }
        return null;
    }
}
